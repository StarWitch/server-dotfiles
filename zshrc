export LANG=en_CA.UTF-8 LC_ALL=en_CA.UTF-8

export PATH=$HOME/.dotfiles/bin:/usr/local/bin:/usr/sbin:$PATH

export ZSH="$HOME/.oh-my-zsh"

export EDITOR="/usr/bin/vim"

ZSH_THEME="rkj-repos"

zstyle ':omz:update' mode auto      # update automatically without asking
zstyle ':omz:update' frequency 30
plugins=(
	git
	debian
	cp
	fzf
	docker
	docker-compose
	dotenv
    ssh-agent
	zsh-interactive-cd
	zsh-navigation-tools
	tmux
	screen
	nmap
)

source $ZSH/oh-my-zsh.sh


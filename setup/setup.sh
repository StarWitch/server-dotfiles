#!/usr/bin/env bash

echo "Basic tools..."
sudo apt install silversearcher-ag curl wget whois vim nfs-common zsh zsh-autosuggestions zsh-syntax-highlighting fzf tmux screen dotenv nmap net-tools mosh htop terminfo kitty-terminfo -y
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "vim setup..."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir -p ~/.vim/undo ~/.vim/swap ~/.vim/backup

$HOME/.dotfiles/setup/link.sh

vim +PlugUpgrade +PlugInstall +PlugUpdate +CocUpdateSync +qall

sudo chsh -s /usr/bin/zsh

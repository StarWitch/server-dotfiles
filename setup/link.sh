#!/usr/bin/env bash

# remove old dotfiles

DOTFILES="zshrc vimrc gitconfig gitignore"

for df in $DOTFILES
do
    if [ -f "$HOME/.$df" ]; then
        echo "Deleting dotfile from $HOME: $df"
        rm -f $HOME/.$df
    fi
    if [ -f "$HOME/.dotfiles/$df" ]; then
        echo "Linking dotfile from dotfile repo: $df"
        ln -sf $HOME/.dotfiles/$df $HOME/.$df
    else
        echo "File not found in dotfile dir: $df"
    fi
done


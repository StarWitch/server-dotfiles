set encoding=utf-8
set fileencodings=utf-8,iso-2022-jp,euc-jp,cp932,latin1
filetype off
set nocompatible

silent!call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
Plug 'beikome/cosme.vim'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'tpope/vim-abolish'
Plug 'mhinz/vim-signify'
Plug 'zhimsel/vim-stay'
Plug 'vim-scripts/PreserveNoEOL'
call plug#end()

syntax on
filetype plugin indent on

" leaders
let mapleader=" "
let maplocalleader=","

set backspace=indent,eol,start
set noshowmode
set nu
set ruler
set list
set wildmenu
set autoindent
" searching
set ignorecase smartcase
" tab settings (battle for the ages)
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set scrolloff=3
set autoread
set showcmd
set showmode
set ttyfast
set hlsearch

set viewoptions=cursor,folds,slash,unix

"undo history
set undofile
set undodir=~/.vim/undo//
set directory=~/.vim/swap//
set backupdir=~/.vim/backup//
noremap <silent> <Leader>fed :e ~/.vimrc<cr>
noremap <silent> <Leader>feR :so ~/.vimrc<cr>
" turn off/on line numbers
noremap <Tab>p :set nu!<CR>
" easy closing of all splits
noremap <Tab>q :qa!<CR>

if has("patch-8.1.1564")
  set signcolumn=number
else
  set signcolumn=yes
endif

" Prevent common mistake of pressing q: instead :q
ca Q q
ca Q! q!

" turn off/on line numbers
nnoremap <Tab>s :set signcolumn=no<CR>

" arrow keys only indent now
nmap <Left> <<
nmap <Right> >>
vmap <Left> <gv
vmap <Right> >gv

" page up/down
noremap <C-h> <Home>
noremap <C-l> <End>
noremap <C-j> <PageUp>
noremap <C-k> <PageDown>

nnoremap <Tab><Left> <C-W><C-H>
nnoremap <Tab><Right> <C-W><C-L>
nnoremap <Tab><Down> <C-W><C-J>
nnoremap <Tab><Up> <C-W><C-K>

" find/replace trailing whitespace
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

command! TrimWhitespace call TrimWhitespace()

noremap <Tab>t :call TrimWhitespace()<CR>

" commenting shortcuts
vmap <Tab>/ gc
nmap <Tab>/ gcc

set termguicolors
colorscheme cosme
set background=dark

" lightline
let g:lightline = {
    \ 'colorscheme': 'cosme',
    \ 'component_function' : {
    \   'fileformat' : 'LightlineFileformat',
    \ },
    \ }

function! LightlineFileformat()
  return &filetype ==# 'netrw' ? '' : &fileformat
endfunction

" netrw + vim-vinegar
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25

" close my cheat-sheet and netrw if last buffers open
aug netrw_close
au!
    au WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == "netrw" | q | endif
    au WinEnter * if winnr('$') == 1 && expand('%:p') =~ ".*vimhelp" | q | endif
aug END

" work around kitty quirk
let &t_ut=''
let g:localvimrc_sandbox = 0
